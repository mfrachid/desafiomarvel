# desafiomarvel
Marvel Challenge

## Tecnologies

* Java 7
* Spring MVC 4
* Spring Boot
* Maven 3
* JUnit
* JAX-RS com Jersey
* Jackson
* Tomcat

## Builds

To generate local builds with maven:
```bash
 mvn clean install
``` 
To generate versioned builds for production and homologation enviroments:
```
  mvn release:clean
  mvn release:prepare
```

## Deploys

After building project, get .war file and run on your current web server:

* Tomcat
```bash
	./startup.sh
```

## Endpoints

Examples from localhost from spring boot

```
  GET http://localhost:8080/api/1.0/character/hulk
  POST http://localhost:8080/api/1.0/character
  	Request: {"characterName": "Hulk"}
```
