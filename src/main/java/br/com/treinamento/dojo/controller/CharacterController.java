package br.com.treinamento.dojo.controller;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.controller.request.CharacterRequest;
import br.com.treinamento.dojo.controller.response.CharacterCollectionResponse;
import br.com.treinamento.dojo.controller.response.MessageResponse;
import br.com.treinamento.dojo.data.StoreData;
import br.com.treinamento.dojo.exception.ValidationException;
import br.com.treinamento.dojo.model.ComicCharacter;
import br.com.treinamento.dojo.model.ComicCharacterData;
import br.com.treinamento.dojo.pojo.ComicCharacterResultWrapper;
import br.com.treinamento.dojo.pojo.StoryResultWrapper;
import br.com.treinamento.dojo.utils.UtilsHelper;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
public class CharacterController {

	@Autowired
	private Environment env;

	@Autowired
	private StoreData dataStore;

	@RequestMapping(value = "/1.0/character", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public HttpEntity<MessageResponse> loadAction(@RequestBody @Valid final CharacterRequest content,
			final BindingResult result) throws ValidationException {

		UtilsHelper.validate(result);

		MessageResponse msg = new MessageResponse("OK", "Comic character successfully inserted");
		HttpStatus status = HttpStatus.OK;
		
		String gateway = env.getProperty("parameter.api.gateway");
		String publicKey = env.getProperty("parameter.public.key");
		String privateKey = env.getProperty("parameter.private.key");
		long timeStamp = System.currentTimeMillis();

		String stringToHash = timeStamp + privateKey + publicKey;
		String hash = DigestUtils.md5DigestAsHex(stringToHash.getBytes());

		String url = gateway + "characters";

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);
		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client.target(url).queryParam("name", content.getCharacterName())
				.queryParam("ts", timeStamp).queryParam("apikey", publicKey).queryParam("hash", hash);

		Builder request = webTarget.request(MediaType.APPLICATION_JSON);

		Response response = request.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException(response.getStatus() + " - Error requesting api data");
		}
		ComicCharacterResultWrapper ccWrapper = response.readEntity(ComicCharacterResultWrapper.class);
		ComicCharacter character = null;
		for (ComicCharacter c : ccWrapper.getData().getResults()) {
			if (c != null) {
				character = c;
				break;
			}
		}
		
		if (character == null) {
			msg.setStatus("NOK");
			msg.setMessage("Comic character not found");
			status = HttpStatus.NOT_FOUND;
		} else {
			url = gateway + "characters/"+character.getId()+"/stories";
			webTarget = client.target(url).queryParam("ts", timeStamp).queryParam("apikey", publicKey).queryParam("hash", hash);
			
			request = webTarget.request(MediaType.APPLICATION_JSON);

			response = request.get();
			if (response.getStatus() != 200) {
				throw new RuntimeException(response.getStatus() + " - Error requesting api data");
			}
			StoryResultWrapper storyWrapper = response.readEntity(StoryResultWrapper.class);
			
			ComicCharacterData ccData = new ComicCharacterData(character.getId(), character.getName(), character.getDescription(), storyWrapper.getData().getResults());
			dataStore.add(ccData);

		}
		msg.add(linkTo(methodOn(CharacterController.class).loadAction(content, result)).withSelfRel());
		msg.add(linkTo(methodOn(CharacterController.class).eventsAction(content.getCharacterName().toLowerCase()))
				.withRel("search"));
		return new ResponseEntity<MessageResponse>(msg, status);
	}

	@RequestMapping(value = "/1.0/character/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public HttpEntity<CharacterCollectionResponse> eventsAction(@PathVariable(value = "name") String name) {

		List<ComicCharacter> characterCollection = dataStore.search(name);

		CharacterCollectionResponse data = new CharacterCollectionResponse("OK", "", characterCollection);
		data.add(linkTo(methodOn(CharacterController.class).eventsAction(name)).withSelfRel());

		return new ResponseEntity<CharacterCollectionResponse>(data, HttpStatus.OK);
	}
}
