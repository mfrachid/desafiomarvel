package br.com.treinamento.dojo.controller.request;

import org.hibernate.validator.constraints.NotEmpty;

public class CharacterRequest {

	@NotEmpty
	private String characterName;

	public String getCharacterName() {
		return characterName;
	}

	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}
	
}
