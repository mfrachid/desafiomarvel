package br.com.treinamento.dojo.controller.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.treinamento.dojo.controller.response.MessageResponse;
import br.com.treinamento.dojo.model.ComicCharacter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CharacterCollectionResponse extends MessageResponse {

	private List<ComicCharacter> characters;

	@JsonCreator
	public CharacterCollectionResponse(@JsonProperty("status") String status, @JsonProperty("message") String message,
			@JsonProperty("characters") List<ComicCharacter> characters) {
		super(status, message);
		this.characters = characters;
	}

	public List<ComicCharacter> getCharacters() {
		return characters;
	}

	public void setCharacters(List<ComicCharacter> characters) {
		this.characters = characters;
	}

}
