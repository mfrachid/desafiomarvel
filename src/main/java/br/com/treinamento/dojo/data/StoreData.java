package br.com.treinamento.dojo.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.treinamento.dojo.model.ComicCharacter;

@Component("dataStore")
@Scope("singleton")
public class StoreData {

	private Map<String, ComicCharacter> characters;

	StoreData() {
		characters = new HashMap<String, ComicCharacter>();
	}

	public List<ComicCharacter> search(String name) {
		List<ComicCharacter> characterCollection = new ArrayList<ComicCharacter>();
		for (Entry<String, ComicCharacter> e : characters.entrySet()) {
			if (e.getKey().startsWith(name.toLowerCase())) {
				characterCollection.add(e.getValue());
			}
		}
		return characterCollection;
	}

	public void add(ComicCharacter character) {
		characters.put(character.getName().toLowerCase(), character);
	}
}
