package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize
public class ComicCharacter {

	private Integer id;
	private String name;
	private String description;

	@JsonCreator
	public ComicCharacter(@JsonProperty("id") Integer id, @JsonProperty("name") String name,
			@JsonProperty("description") String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public ComicCharacter() {
	}

	@JsonGetter("id")
	public Integer getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonGetter("name")
	public String getName() {
		return name;
	}

	@JsonSetter("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonGetter("description")
	public String getDescription() {
		return description;
	}

	@JsonSetter("description")
	public void setDescription(String description) {
		this.description = description;
	}

}
