package br.com.treinamento.dojo.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

public class ComicCharacterData extends ComicCharacter {

	private List<Story> stories;
	
	@JsonCreator
	public ComicCharacterData(@JsonProperty("id") Integer id, @JsonProperty("name") String name,
			@JsonProperty("description") String description, @JsonProperty("stories") List<Story> stories) {
		super(id, name, description);
		this.stories = stories;
	}
	
	@JsonGetter("stories")
	public List<Story> getStories() {
		return stories;
	}
	
	@JsonSetter("stories")
	public void setStories(List<Story> stories) {
		this.stories = stories;
	}
}
