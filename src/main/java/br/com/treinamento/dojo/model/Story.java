package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Story {

	private Integer id;
	private String title;
	private String description;
	private String type;

	@JsonCreator
	public Story(@JsonProperty("id") Integer id, @JsonProperty("title") String title,
			@JsonProperty("description") String description, @JsonProperty("type") String type) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.type = type;
	}

	public Story() {
	}

	@JsonGetter("id")
	public Integer getId() {
		return id;
	}

	@JsonSetter("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonGetter("title")
	public String getTitle() {
		return title;
	}

	@JsonSetter("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonGetter("description")
	public String getDescription() {
		return description;
	}

	@JsonSetter("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonGetter("type")
	public String getType() {
		return type;
	}

	@JsonSetter("type")
	public void setType(String type) {
		this.type = type;
	}

}
