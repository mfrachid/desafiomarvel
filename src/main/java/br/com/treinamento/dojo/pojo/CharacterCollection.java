package br.com.treinamento.dojo.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.treinamento.dojo.model.ComicCharacter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize
public class CharacterCollection {

	private List<ComicCharacter> results;

	public List<ComicCharacter> getResults() {
		return results;
	}

	public void setResults(List<ComicCharacter> results) {
		this.results = results;
	}

	

}
