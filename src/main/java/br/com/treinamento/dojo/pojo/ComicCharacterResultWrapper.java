package br.com.treinamento.dojo.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComicCharacterResultWrapper {
	
    private CharacterCollection data;
    
	public CharacterCollection getData() {
		return data;
	}
	public void setData(CharacterCollection data) {
		this.data = data;
	}
    
}
