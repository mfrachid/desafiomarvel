package br.com.treinamento.dojo.pojo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.treinamento.dojo.model.Story;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize
public class StoryCollection {

	private List<Story> results;

	public List<Story> getResults() {
		return results;
	}

	public void setResults(List<Story> results) {
		this.results = results;
	}

	

}
