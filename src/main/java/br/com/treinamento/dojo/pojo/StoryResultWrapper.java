package br.com.treinamento.dojo.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StoryResultWrapper {
	
    private StoryCollection data;
    
	public StoryCollection getData() {
		return data;
	}
	public void setData(StoryCollection data) {
		this.data = data;
	}
    
}
