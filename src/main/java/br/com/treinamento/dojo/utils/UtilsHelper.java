package br.com.treinamento.dojo.utils;

import org.springframework.validation.BindingResult;

import br.com.treinamento.dojo.exception.ValidationException;

public class UtilsHelper {

	public static void validate(final BindingResult result) throws ValidationException {
		if (result.hasErrors()) {
			throw new ValidationException(result);
		}
	}
	
}
