package desafiomarvel;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.controller.request.CharacterRequest;
import br.com.treinamento.dojo.controller.response.CharacterCollectionResponse;
import br.com.treinamento.dojo.controller.response.MessageResponse;
import br.com.treinamento.dojo.model.ComicCharacter;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class, loader=SpringApplicationContextLoader.class)
@WebIntegrationTest
public class CharacterControllerTest {
	
	@Value("${local.server.port}")
    int port;

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void emptyStorageTest() throws Exception
	{
		ResponseEntity<CharacterCollectionResponse> entity = this.restTemplate.getForEntity("http://localhost:" + port + "/api/1.0/character/{name}", CharacterCollectionResponse.class, "hulk");
		assertThat(entity.getStatusCode(), is(HttpStatus.OK));
		CharacterCollectionResponse collection = entity.getBody();
		assertThat(collection.getCharacters(), is(hasSize(0)));
	}
	
	@Test
	public void hulkTest() throws Exception
	{
		CharacterRequest newCharacter = new CharacterRequest();
		newCharacter.setCharacterName("hulk");
		ResponseEntity<MessageResponse> addEntity = this.restTemplate.postForEntity("http://localhost:" + port + "/api/1.0/character", newCharacter, MessageResponse.class);
		assertThat(addEntity.getStatusCode(), is(HttpStatus.OK));
		MessageResponse msg = addEntity.getBody();
		assertEquals(msg.getStatus(), "OK");
		assertEquals(msg.getMessage(), "Comic character successfully inserted");
		
		ResponseEntity<CharacterCollectionResponse> searchEntity = this.restTemplate.getForEntity("http://localhost:" + port + "/api/1.0/character/{name}", CharacterCollectionResponse.class, "hulk");
		assertThat(searchEntity.getStatusCode(), is(HttpStatus.OK));
		CharacterCollectionResponse collection = searchEntity.getBody();
		List<ComicCharacter> list = collection.getCharacters();
		assertThat(list, is(hasSize(1)));
		ComicCharacter hulk = null;
		for (ComicCharacter character : list)
		{
			if (character.getName().toLowerCase().equals("hulk"))
				hulk = character;
		}
		assertNotNull(hulk);
	}
	
	@Test
	public void wolverineTest() throws Exception
	{
		CharacterCollectionResponse collection = null;
		
		// Does not find wolverine
		ResponseEntity<CharacterCollectionResponse> entity = this.restTemplate.getForEntity("http://localhost:" + port + "/api/1.0/character/{name}", CharacterCollectionResponse.class, "wolverine");
		assertThat(entity.getStatusCode(), is(HttpStatus.OK));
		collection = entity.getBody();
		assertThat(collection.getCharacters(), is(hasSize(0)));
		
		// Add wolverine
		CharacterRequest newCharacter = new CharacterRequest();
		newCharacter.setCharacterName("wolverine");
		ResponseEntity<MessageResponse> addEntity = this.restTemplate.postForEntity("http://localhost:" + port + "/api/1.0/character", newCharacter, MessageResponse.class);
		assertThat(addEntity.getStatusCode(), is(HttpStatus.OK));
		MessageResponse msg = addEntity.getBody();
		assertEquals(msg.getStatus(), "OK");
		assertEquals(msg.getMessage(), "Comic character successfully inserted");
		
		// Finds wolverine
		ResponseEntity<CharacterCollectionResponse> searchEntity = this.restTemplate.getForEntity("http://localhost:" + port + "/api/1.0/character/{name}", CharacterCollectionResponse.class, "wolverine");
		assertThat(searchEntity.getStatusCode(), is(HttpStatus.OK));
		collection = searchEntity.getBody();
		List<ComicCharacter> list = collection.getCharacters();
		assertThat(list, is(hasSize(1)));
		ComicCharacter wolverine = null;
		for (ComicCharacter character : list)
		{
			if (character.getName().toLowerCase().equals("wolverine"))
				wolverine = character;
		}
		assertNotNull(wolverine);
	}
	
}
